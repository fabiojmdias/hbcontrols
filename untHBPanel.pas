//************************ www.hackbra.in ****************************//
//                                                                    //
//               THbPanel - a transparent panel (VCL)                 //
//                                                                    //
// The Initial Developer of the Original Code is dias@hackbra.in      //
//********************************************************************//
unit untHBPanel;

interface

uses
  Vcl.ExtCtrls, Vcl.Controls, Winapi.Messages, Winapi.Windows, System.Classes;

type
  THBPanel = class(Vcl.ExtCtrls.TPanel)
  private
    procedure CnCtlColorStatic(var Msg: TWMCtlColorStatic); message CN_CTLCOLORSTATIC;
    procedure WmEraseBkgnd(var Msg: TWMEraseBkgnd); message WM_ERASEBKGND;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure Paint; override;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('HBControls', [THBPanel]);
end;


{ THBPanel }

procedure THBPanel.CnCtlColorStatic(var Msg: TWMCtlColorStatic);
begin
  SetBKMode(Msg.ChildDC, TRANSPARENT);
  Msg.Result := GetStockObject(NULL_BRUSH);
end;

procedure THBPanel.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  Params.ExStyle := Params.ExStyle or WS_EX_TRANSPARENT;
end;

procedure THBPanel.Paint;
begin
  SetBkMode(Handle, TRANSPARENT);
  //inherited;
end;

procedure THBPanel.WmEraseBkgnd(var Msg: TWMEraseBkgnd);
begin
  Msg.Result := 1;
end;

end.
